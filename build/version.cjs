const semver = require('semver');
const package = require('../package.json');
const version = semver.coerce(process.version).toString();
const { strict: assert } = require('assert');
const spec = package.engines.node;
const minVersion = semver.minVersion(spec).toString();

assert.equal(
	version,
	minVersion,
	`Wrong version of node:
		You should use node version ${minVersion}, the minimum that satisfies the spec in package.json (${spec}).
		You are using ${version}.`
);
