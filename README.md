# Liquid Assets

A library for dealing with assets without requiring any build tools.

## Overview 

This library comprises of three things:

1. A class to represent assets
2. A class to generate URLs for assets (and find an asset given a URL)
3. A function to serve assets over Node's http server

The following example shows all of these parts in use:

```javascript
import { asset, AssetMap, assetRequestHandler } from 'liquid-assets';

// Reference an asset relative to this file (1)
const profilePic = asset('./profile.jpg', import.meta.url);

// Keep a bidirectional map of Asset objects and URLS (2)
const mapper = await AssetMap.register('http://example.com/assets/', [profilePic]);

// Create a function that serves the assets (3)
const serveAsset = assetRequestHandler(mapper);

export default async function requestHandler(req, res) {
    // Serve any registered assets
    if(await serveAsset(req, res)) {
        // An asset was found and served; exit the request handler
        return;
    }

    // No asset was found; serve the default route
    return `<html><body>
        <h1>Mr Example</h1>
        <p>Hello, my name is Mr. Example. Here's a picture of me:</p>
        <img src="${
            // Get the URL for the asset from the asset map
            await mapper.getUrl(profilePic);
        }">`

}
```

## Usage patterns

I've tried to make this library simple enough to use in whatever configuration is needed:

### Webpack-style

If you're used to doing `import assetUrl from './asset.jpg';`, you might find the following pattern useful:

First, set up your mapper in its own file:

```javascript
import { AssetMap, Asset, assetRequestHandler } from 'liquid-assets';

export const mapper = new AssetMap('http://example.com/assets/');

// Make sure you use this in your server callback so the assets are actually served
export const serveAssets = assetRequestHandler(mapper);

export default (...args) => mapper.getUrl(new Asset(...args));
```

Then, whenever you need to reference an asset in another file, import the asset mapper and use it to generate a URL:

```javascript
import urlFor from './asset-mapper.js';

const virus = await urlFor('./virus.exe', import.meta.url);

export default `<a href="${virus}">Click here</a> for a picture of a cute panda!`;
```

### Template-first

If you want to be able to specify assets in your templates without having to have a mapper set up first,
that's possible too using a library like `encode-html-template-tag` that can replace assets at render time:

```javascript
import { asset } from 'liquid-assets';
import html from 'encode-html-template-tag';

export const vom = asset('./vom.ogg', import.meta.url);

export default html`
This is what my cat sounds like when she's about to throw up:
<audio src="${vom}"></audio>`;
```

Then, in your server code, you can replace the asset with its URL at render time:

```javascript
import html from 'encode-html-template-tag';
import { AssetMap, assetRequestHandler, Asset } from 'liquid-assets';
import vomTemplate, { vom } from './cat-vomit.js';

const notFound = html`That page was not found`;

export default async function(origin = 'http://example.com') {
    const assets = await AssetMap.register(`${origin}/assets/`, [vom]);
    const assetServer = assetRequestHandler(assets);

    return async function(req, res) {
        if(await assetServer(req, res)) {
            return;
        }

        // Imagine this is your router function
        const template = req.url === '/' ? vomTemplate : notFound;

        // Render the output and replace Assets with their URLs
        const output = await template.render(value => {
            if(value instanceof Asset) {
                // Get the asset URL
                const url = await assets.getUrl(value);

                // We can also make the URL relative if we want
                return url.toString().replace(origin, '');
            }

            return value;
        });
    }
}

```

## Dependencies
 - mime-types: ^2.1.33
## Modules

<dl>
<dt><a href="#module_liquid-assets">liquid-assets</a></dt>
<dd><p>Liquid Assets</p>
</dd>
<dt><a href="#module_liquid-assets">liquid-assets</a></dt>
<dd><p>Liquid Assets</p>
</dd>
</dl>

## Classes

<dl>
<dt><a href="#AssetMap">AssetMap</a></dt>
<dd><p>Generates URLs for assets, and maintains a map of those URLs to their asset objects.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#assetRequestHandler">assetRequestHandler(assetMapper)</a> ⇒ <code>function</code></dt>
<dd><p>Create a request handler for serving assets.
Note that the request handler expects asset URLs to be content-addressed,
and therefore sets the cache-control header to <code>immutable</code>.
If you don&#39;t want this behaviour, don&#39;t use this function.</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#AssetMapOptions">AssetMapOptions</a> : <code>object</code></dt>
<dd><p>Options for constructing the AssetMap</p>
</dd>
<dt><a href="#RequestHandler">RequestHandler</a> ⇒ <code>Boolean</code></dt>
<dd><p>Asset request handler</p>
</dd>
</dl>

<a name="module_liquid-assets"></a>

## liquid-assets
Liquid Assets


* [liquid-assets](#module_liquid-assets)
    * [.Asset](#module_liquid-assets.Asset)
        * [new exports.Asset(options)](#new_module_liquid-assets.Asset_new)
        * [.size](#module_liquid-assets.Asset+size) ⇒ <code>number</code>
        * [.type](#module_liquid-assets.Asset+type) ⇒ <code>string</code>
        * [.name](#module_liquid-assets.Asset+name) ⇒ <code>string</code>
        * [.ext](#module_liquid-assets.Asset+ext) ⇒ <code>string</code>
        * [.Symbol.asyncIterator()](#module_liquid-assets.Asset+Symbol.asyncIterator)
        * [.toString()](#module_liquid-assets.Asset+toString) ⇒ <code>string</code>
    * [.FileAsset](#module_liquid-assets.FileAsset)
        * [new exports.FileAsset(file, baseUrl)](#new_module_liquid-assets.FileAsset_new)
    * [.InlineAsset](#module_liquid-assets.InlineAsset)
        * [new exports.InlineAsset(blob, type, name)](#new_module_liquid-assets.InlineAsset_new)
    * [.asset](#module_liquid-assets.asset) ⇒ <code>FileAsset</code>

<a name="module_liquid-assets.Asset"></a>

### liquid-assets.Asset
A generic representation of an asset

**Kind**: static class of [<code>liquid-assets</code>](#module_liquid-assets)  

* [.Asset](#module_liquid-assets.Asset)
    * [new exports.Asset(options)](#new_module_liquid-assets.Asset_new)
    * [.size](#module_liquid-assets.Asset+size) ⇒ <code>number</code>
    * [.type](#module_liquid-assets.Asset+type) ⇒ <code>string</code>
    * [.name](#module_liquid-assets.Asset+name) ⇒ <code>string</code>
    * [.ext](#module_liquid-assets.Asset+ext) ⇒ <code>string</code>
    * [.Symbol.asyncIterator()](#module_liquid-assets.Asset+Symbol.asyncIterator)
    * [.toString()](#module_liquid-assets.Asset+toString) ⇒ <code>string</code>

<a name="new_module_liquid-assets.Asset_new"></a>

#### new exports.Asset(options)
Create a new asset


| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Options |
| options.type | <code>string</code> | The mime-type of the asset |
| options.toStream | <code>function</code> | A function that returns an async iterable yeilding the asset's contents |
| options.toString | <code>function</code> | A function that returns the asset represented as a data url |
| options.name | <code>string</code> | An optional filename for the asset |
| options.size | <code>number</code> | The asset size in bytes |

<a name="module_liquid-assets.Asset+size"></a>

#### asset.size ⇒ <code>number</code>
Get the asset's file size in bytes

**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
<a name="module_liquid-assets.Asset+type"></a>

#### asset.type ⇒ <code>string</code>
Get the asset's mime type

**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
<a name="module_liquid-assets.Asset+name"></a>

#### asset.name ⇒ <code>string</code>
**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
**Returns**: <code>string</code> - The filename of the asset  
<a name="module_liquid-assets.Asset+ext"></a>

#### asset.ext ⇒ <code>string</code>
**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
**Returns**: <code>string</code> - The extname of the asset  
<a name="module_liquid-assets.Asset+Symbol.asyncIterator"></a>

#### asset.Symbol.asyncIterator()
Generator that yields the asset's contents

**Kind**: instance method of [<code>Asset</code>](#module_liquid-assets.Asset)  
<a name="module_liquid-assets.Asset+toString"></a>

#### asset.toString() ⇒ <code>string</code>
**Kind**: instance method of [<code>Asset</code>](#module_liquid-assets.Asset)  
**Returns**: <code>string</code> - Data URL of the asset  
<a name="module_liquid-assets.FileAsset"></a>

### liquid-assets.FileAsset
An asset on the filesystem

**Kind**: static class of [<code>liquid-assets</code>](#module_liquid-assets)  
<a name="new_module_liquid-assets.FileAsset_new"></a>

#### new exports.FileAsset(file, baseUrl)
Create a new FileAsset


| Param | Type | Description |
| --- | --- | --- |
| file | <code>string</code> | Absolute or relative path of the file |
| baseUrl | <code>string</code> | The base file:// URL to calculate relative path from |

<a name="module_liquid-assets.InlineAsset"></a>

### liquid-assets.InlineAsset
An asset declared in code, stored in application memory

**Kind**: static class of [<code>liquid-assets</code>](#module_liquid-assets)  
<a name="new_module_liquid-assets.InlineAsset_new"></a>

#### new exports.InlineAsset(blob, type, name)
Create a new InlineAsset


| Param | Type | Description |
| --- | --- | --- |
| blob | <code>Buffer</code> \| <code>string</code> | The contents of the asset |
| type | <code>string</code> | The mimetype of the asset |
| name | <code>string</code> | Optional filename for the asset |

<a name="module_liquid-assets.asset"></a>

### liquid-assets.asset ⇒ <code>FileAsset</code>
Create a new FileAsset

**Kind**: static constant of [<code>liquid-assets</code>](#module_liquid-assets)  

| Param | Type | Description |
| --- | --- | --- |
| file | <code>string</code> | Absolute or relative path of the file |
| baseUrl | <code>string</code> | The base file:// URL to calculate relative path from |

<a name="module_liquid-assets"></a>

## liquid-assets
Liquid Assets


* [liquid-assets](#module_liquid-assets)
    * [.Asset](#module_liquid-assets.Asset)
        * [new exports.Asset(options)](#new_module_liquid-assets.Asset_new)
        * [.size](#module_liquid-assets.Asset+size) ⇒ <code>number</code>
        * [.type](#module_liquid-assets.Asset+type) ⇒ <code>string</code>
        * [.name](#module_liquid-assets.Asset+name) ⇒ <code>string</code>
        * [.ext](#module_liquid-assets.Asset+ext) ⇒ <code>string</code>
        * [.Symbol.asyncIterator()](#module_liquid-assets.Asset+Symbol.asyncIterator)
        * [.toString()](#module_liquid-assets.Asset+toString) ⇒ <code>string</code>
    * [.FileAsset](#module_liquid-assets.FileAsset)
        * [new exports.FileAsset(file, baseUrl)](#new_module_liquid-assets.FileAsset_new)
    * [.InlineAsset](#module_liquid-assets.InlineAsset)
        * [new exports.InlineAsset(blob, type, name)](#new_module_liquid-assets.InlineAsset_new)
    * [.asset](#module_liquid-assets.asset) ⇒ <code>FileAsset</code>

<a name="module_liquid-assets.Asset"></a>

### liquid-assets.Asset
A generic representation of an asset

**Kind**: static class of [<code>liquid-assets</code>](#module_liquid-assets)  

* [.Asset](#module_liquid-assets.Asset)
    * [new exports.Asset(options)](#new_module_liquid-assets.Asset_new)
    * [.size](#module_liquid-assets.Asset+size) ⇒ <code>number</code>
    * [.type](#module_liquid-assets.Asset+type) ⇒ <code>string</code>
    * [.name](#module_liquid-assets.Asset+name) ⇒ <code>string</code>
    * [.ext](#module_liquid-assets.Asset+ext) ⇒ <code>string</code>
    * [.Symbol.asyncIterator()](#module_liquid-assets.Asset+Symbol.asyncIterator)
    * [.toString()](#module_liquid-assets.Asset+toString) ⇒ <code>string</code>

<a name="new_module_liquid-assets.Asset_new"></a>

#### new exports.Asset(options)
Create a new asset


| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Options |
| options.type | <code>string</code> | The mime-type of the asset |
| options.toStream | <code>function</code> | A function that returns an async iterable yeilding the asset's contents |
| options.toString | <code>function</code> | A function that returns the asset represented as a data url |
| options.name | <code>string</code> | An optional filename for the asset |
| options.size | <code>number</code> | The asset size in bytes |

<a name="module_liquid-assets.Asset+size"></a>

#### asset.size ⇒ <code>number</code>
Get the asset's file size in bytes

**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
<a name="module_liquid-assets.Asset+type"></a>

#### asset.type ⇒ <code>string</code>
Get the asset's mime type

**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
<a name="module_liquid-assets.Asset+name"></a>

#### asset.name ⇒ <code>string</code>
**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
**Returns**: <code>string</code> - The filename of the asset  
<a name="module_liquid-assets.Asset+ext"></a>

#### asset.ext ⇒ <code>string</code>
**Kind**: instance property of [<code>Asset</code>](#module_liquid-assets.Asset)  
**Returns**: <code>string</code> - The extname of the asset  
<a name="module_liquid-assets.Asset+Symbol.asyncIterator"></a>

#### asset.Symbol.asyncIterator()
Generator that yields the asset's contents

**Kind**: instance method of [<code>Asset</code>](#module_liquid-assets.Asset)  
<a name="module_liquid-assets.Asset+toString"></a>

#### asset.toString() ⇒ <code>string</code>
**Kind**: instance method of [<code>Asset</code>](#module_liquid-assets.Asset)  
**Returns**: <code>string</code> - Data URL of the asset  
<a name="module_liquid-assets.FileAsset"></a>

### liquid-assets.FileAsset
An asset on the filesystem

**Kind**: static class of [<code>liquid-assets</code>](#module_liquid-assets)  
<a name="new_module_liquid-assets.FileAsset_new"></a>

#### new exports.FileAsset(file, baseUrl)
Create a new FileAsset


| Param | Type | Description |
| --- | --- | --- |
| file | <code>string</code> | Absolute or relative path of the file |
| baseUrl | <code>string</code> | The base file:// URL to calculate relative path from |

<a name="module_liquid-assets.InlineAsset"></a>

### liquid-assets.InlineAsset
An asset declared in code, stored in application memory

**Kind**: static class of [<code>liquid-assets</code>](#module_liquid-assets)  
<a name="new_module_liquid-assets.InlineAsset_new"></a>

#### new exports.InlineAsset(blob, type, name)
Create a new InlineAsset


| Param | Type | Description |
| --- | --- | --- |
| blob | <code>Buffer</code> \| <code>string</code> | The contents of the asset |
| type | <code>string</code> | The mimetype of the asset |
| name | <code>string</code> | Optional filename for the asset |

<a name="module_liquid-assets.asset"></a>

### liquid-assets.asset ⇒ <code>FileAsset</code>
Create a new FileAsset

**Kind**: static constant of [<code>liquid-assets</code>](#module_liquid-assets)  

| Param | Type | Description |
| --- | --- | --- |
| file | <code>string</code> | Absolute or relative path of the file |
| baseUrl | <code>string</code> | The base file:// URL to calculate relative path from |

<a name="assetRequestHandler"></a>

## assetRequestHandler(assetMapper) ⇒ <code>function</code>
Create a request handler for serving assets.
Note that the request handler expects asset URLs to be content-addressed,
and therefore sets the cache-control header to `immutable`.
If you don't want this behaviour, don't use this function.

**Kind**: global function  
**Returns**: <code>function</code> - Request handling function  

| Param | Type | Description |
| --- | --- | --- |
| assetMapper | <code>AssetMapper</code> | The asset mapper containing the assets to serve |

<a name="AssetMapOptions"></a>

## AssetMapOptions : <code>object</code>
Options for constructing the AssetMap

**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| options.algorithm | <code>string</code> | The algorithm to use when generating the filename hash; default sha1 |
| options.length | <code>number</code> | How many characters in length the hash part of the URL should be; default 7 |
| options.minSize | <code>number</code> | The minimum size the asset needs to be for an http URL to be generated. 		Anything below this size will be converted as a data: URL |

<a name="RequestHandler"></a>

## RequestHandler ⇒ <code>Boolean</code>
Asset request handler

**Kind**: global typedef  
**Returns**: <code>Boolean</code> - True if an asset was sent, false if no matching asset was found  

| Param | Type | Description |
| --- | --- | --- |
| req | <code>IncomingMessage</code> | The Node incoming request object |
| res | <code>OutgoingResponse</code> | The Node HTTP response object |

