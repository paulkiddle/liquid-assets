import { expect, jest, test } from '@jest/globals';
import { assetRequestHandler as assetHandler } from './responder.js';
import { Writable } from 'stream';

class Res extends Writable {
	constructor(){
		const written = [];
		super({ write(chunk, enc, cb) { written.push(chunk); cb(); } });
		
		this.written = {
			data: written,
			headers: {}
		};
	}

	setHeader(key, value){
		this.written.headers[key] = value;
	}
}

function getHandler(asset){
	const getAsset = jest.fn(()=>asset);
	const handler = assetHandler({ getAsset });
	return handler;
}

test('asset handler no asset', async()=>{
	const handler = getHandler();
	const req = { url: '/asset.jpg' };
	const res = new Res;

	expect(await handler(req, res)).toBe(false);
	expect(res.written).toMatchSnapshot();
});

test('asset handler w/ asset', async()=>{
	const asset = ['a','b','c'];
	const handler = getHandler(asset);
	const req = { url: '/asset.jpg' };
	const res = new Res;

	expect(await handler(req, res)).toBe(true);
	expect(res.written).toMatchSnapshot();
});


test('asset handler w/ sized asset', async()=>{
	const asset = ['a','b','c'];
	asset.size = 3;
	const handler = getHandler(asset);
	const req = { url: '/asset.jpg' };
	const res = new Res;
	expect(await handler(req, res)).toBe(true);
	expect(res.written).toMatchSnapshot();
});