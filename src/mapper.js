import Crypto from 'crypto';
import { pipeline } from 'stream/promises';
import path from 'path';
import mime from 'mime-types';
import { FileAsset } from './asset.js';

const getExtension = type => {
	const ext = mime.extension(type);
	if(ext) {
		return '.' + ext;
	}

	return '';
};

/**
 * Options for constructing the AssetMap
 * @typedef	{object}	AssetMapOptions
 * @property	{string}	options.algorithm	The algorithm to use when generating the filename hash; default sha1
 * @property	{number}	options.length	How many characters in length the hash part of the URL should be; default 7
 * @property	{number}	options.minSize	The minimum size the asset needs to be for an http URL to be generated.
 *		Anything below this size will be converted as a data: URL 
 */

/**
 * Generates URLs for assets, and maintains a map of those URLs to their asset objects.
 */
export class AssetMap {
	#assets = new Map;
	#urls = new Map;
	#baseUrl;
	#hashAlg;
	#hashLength;
	#minSize;

	/**
	 * 
	 * @param {string|URL}	baseUrl	The base URL for the generated asset URLs. Must end with `/`
	 * @param {AssetMapOptions}	options	Options for URL generation
	 */
	constructor(baseUrl, { algorithm = 'sha1', length = 7, minSize=8000000 } = {}){
		this.#baseUrl = baseUrl;
		this.#hashAlg = algorithm;
		this.#hashLength = length;
		this.#minSize = minSize;
	}

	/**
	 * Create a new AssetMap and register assets on it
	 * @param	{string|URL}	baseUrl	The base URL for the generated asset URLs. Must end with `/`
	 * @param	{Asset[]}	assets	Array of assets to register
	 * @param	{AssetMapOptions}	options	Options for URL generation
	 * @returns	{AssetMap}	The created map
	 */
	static async register(baseUrl, assets, options) {
		const m = new AssetMap(baseUrl, options);

		await m.register(assets);

		return m;
	}

	async #generateName(asset){
		const hasher = Crypto.createHash(this.#hashAlg);
		hasher.setEncoding('hex');

		await pipeline(asset, hasher);

		const hash = hasher.read().slice(0, this.#hashLength);
		
		const ext = path.extname('_' + asset.ext) || getExtension(asset.type);

		const name = asset.name ? path.basename(asset.name) : '';

		const filename = path.join(hash, name) + ext;

		return filename;
	}

	async #generateUrl(asset){
		if(asset.size && asset.size < this.#minSize) {
			return asset.toString();
		}

		const filename = await this.#generateName(asset);
		const url = new URL(filename, this.#baseUrl);
		this.#assets.set(url.pathname, asset);
		return url;
	}

	async #register(asset){
		const url = await this.#generateUrl(asset);

		this.#urls.set(asset, url);
		return url;
	}

	/**
	 * Register multiple assets
	 * @param {Asset[]} assets Assets to register
	 */
	async register(assets) {
		for(const asset of assets) {
			await this.#register(asset);
		}
	}

	/**
	 * Check if an asset has been registered in the mapper
	 * @param	{Asset}	asset The asset to check for
	 * @returns	{Boolean}	True if the asset is present
	 */
	has(asset) {
		return this.#urls.has(asset);
	}

	/**
	 * Generate and return a URL for an asset
	 * @param	{Asset}	asset The asset to get a URL for
	 * @returns	{string|URL}	A URL representing the asset
	 */
	async getUrl(asset, base) {
		if(typeof asset === 'string') {
			asset = new FileAsset(asset, base);
		}
	
		if(this.has(asset)) {
			return this.#urls.get(asset);
		}

		return await this.#register(asset);
	}

	/**
	 * Get the assset object that generated a given URL
	 * @param	{string|URL}	url The URl for the asset
	 * @returns	{Asset}	The asset
	 */
	getAsset(url) {
		const { pathname } = new URL(url, 'file://');
		return this.#assets.get(pathname);
	}
}