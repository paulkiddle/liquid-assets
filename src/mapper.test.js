import { expect, test } from '@jest/globals';
import {AssetMap} from './mapper.js';

test('asset mapper', async()=>{
	const base = 'http://example.com/assets/';
	const asset = ['test'];
	const mapper = await AssetMap.register(base, [asset]);

	expect(mapper.getAsset(new URL('a94a8fe', base))).toBe(asset);
	expect(await mapper.getUrl(asset)).toEqual(new URL('a94a8fe', base));

	const a2 = Object.assign(['test'], { type: 'text/plain' });
	expect(await mapper.getUrl(a2)).toEqual(new URL('a94a8fe.txt', base));
	expect(mapper.getAsset('/assets/a94a8fe.txt')).toBe(a2);

	const a3 = Object.assign(['test'], { name: 'example.jpg' });
	expect(await mapper.getUrl(a3)).toEqual(new URL('a94a8fe/example.jpg', base));

	const a4 = Object.assign(['test'], { type: 'text/plain', size: '4', toString(){ return 'data:,test'; }});
	expect(await mapper.getUrl(a4)).toEqual('data:,test');

	expect(await mapper.getUrl('./fixtures/resource.test.txt', import.meta.url)).toMatchSnapshot();
}); 