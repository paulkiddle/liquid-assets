import { expect, test } from '@jest/globals';
import { asset } from './asset.js';

async function testAsset(testFile) {
	const { name, type, ext, size } = testFile;
	expect({ name, type, ext, size}).toMatchSnapshot();
	expect(testFile.toString()).toMatchSnapshot();

	const data = [];

	for await(const chunk of testFile) {
		data.push(chunk);
	}
	expect(Buffer.concat(data).toString('base64')).toMatchSnapshot();
}

test('File asset', async()=> {
	const testFile = asset('fixtures/resource.test.txt', import.meta.url);

	await testAsset(testFile);

	const testFile2 = asset('fixtures/other', import.meta.url);

	await testAsset(testFile2);
});

test('Inline asset', async()=> {
	const testFile = asset.inline('"Test string"', 'application/json', 'my-file.json');

	await testAsset(testFile);

	const testFile2 = asset.inline(Buffer.from('misc content'));

	await testAsset(testFile2);
});
