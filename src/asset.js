import mime from 'mime-types';
import fs from 'fs';
import path from 'path';

/**
 * Liquid Assets
 * @module liquid-assets
 */

/**
 * A generic representation of an asset
 */
export class Asset {
	#options;

	/**
	 * Create a new asset
	 * @param {Object}	options	Options
	 * @param {string}	options.type	The mime-type of the asset
	 * @param {Function}	options.toStream	A function that returns an async iterable yeilding the asset's contents
	 * @param {Function}	options.toString	A function that returns the asset represented as a data url
	 * @param {string}	options.name	An optional filename for the asset
	 * @param {number}	options.size	The asset size in bytes
	 */
	constructor({ type, toStream, toString, name, size }){
		this.#options = { type, toStream, toString, name, size };
	}

	/**
	 * Get the asset's file size in bytes
	 * @return {number}
	 */
	get size(){
		return this.#options.size;
	}

	/**
	 * Get the asset's mime type
	 * @return {string}
	 */
	get type(){
		return this.#options.type;
	}

	/**
	 * Generator that yields the asset's contents
	 * @yield {Buffer} The next chunk of contents
	 */
	async * [Symbol.asyncIterator](){
		yield* this.#options.toStream();
	}

	/**
	 * @returns {string} Data URL of the asset
	 */
	toString(){
		return this.#options.toString();
	}

	/**
	 * @return {string} The filename of the asset
	 */
	get name(){
		return this.#options.name || '';
	}

	/**
	 * @return {string} The extname of the asset
	 */
	get ext(){
		return (this.name && path.extname(this.name)) || '';
	}
}

const dataUrl = (type, buffer) => 'data:' + type + ';base64,' + buffer.toString('base64');

const defaultType = 'application/octet-stream';

/**
 * An asset on the filesystem
 */
export class FileAsset extends Asset {
	/**
	 * Create a new FileAsset
	 * @param {string}	file Absolute or relative path of the file
	 * @param {string?}	baseUrl The base file:// URL to calculate relative path from
	 */
	constructor(file, baseUrl) {
		const { pathname } = new URL(file, baseUrl);
		const type =  mime.lookup(pathname) || defaultType;

		super({
			type,
			toStream(){
				return fs.createReadStream(pathname);
			},
			toString() {
				return dataUrl(type, fs.readFileSync(pathname));
			},
			name: path.basename(pathname),
			size: fs.statSync(pathname).size
		});
	}
}

/**
 * An asset declared in code, stored in application memory
 */
export class InlineAsset extends Asset {
	/**
	 * Create a new InlineAsset
	 * @param {Buffer|string}	blob The contents of the asset
	 * @param {string}	type The mimetype of the asset
	 * @param {string}	name Optional filename for the asset
	 */
	constructor(blob, type = defaultType, name) {
		const buffer = Buffer.from(blob);

		super({
			type,
			async * toStream(){
				yield buffer;
			},
			toString(){
				return dataUrl(type, buffer);
			},
			name,
			size: buffer.length
		});
	}
}

/**
 * Create a new FileAsset
 * @param	{string}	file Absolute or relative path of the file
 * @param	{string?}	baseUrl The base file:// URL to calculate relative path from
 * @returns	{FileAsset}
 */
export const asset = (...args)=>new FileAsset(...args);

/**
	 * Create a new InlineAsset
	 * @param	{Buffer|string}	blob The contents of the asset
	 * @param	{string}	type The mimetype of the asset
	 * @param	{string}	name Optional filename for the asset
	 * @returns	{InlineAsset}
	 */
asset.inline = (...args) => new InlineAsset(...args);