import { pipeline } from 'stream/promises';

/**
 * Create a request handler for serving assets.
 * Note that the request handler expects asset URLs to be content-addressed,
 * and therefore sets the cache-control header to `immutable`.
 * If you don't want this behaviour, don't use this function.
 * @param	{AssetMapper}	assetMapper The asset mapper containing the assets to serve
 * @returns	{function}	Request handling function
 */
export function assetRequestHandler(assetMapper){
	/**
	 * Asset request handler
	 * @callback	RequestHandler
	 * @param	{IncomingMessage}	req	The Node incoming request object
	 * @param	{OutgoingResponse}	res The Node HTTP response object
	 * @returns	{Boolean}	True if an asset was sent, false if no matching asset was found
	 */
	return async function (req, res) {
		const asset = assetMapper.getAsset(req.url);

		if(!asset){
			return false;
		}

		await sendAsset(res, asset);

		return true;
	};
}

async function sendAsset(res, asset) {
	res.setHeader('content-type', asset.type);
	res.setHeader('cache-control', 'immutable');
	if(asset.size) {
		res.setHeader('content-length', asset.size);
	}
	await pipeline(asset, res);
}